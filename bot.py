import discord
from discord.ext import commands
import traceback
import datetime
import requests
import asyncio
import espnscrape
from pymongo import MongoClient
client = MongoClient('mongodb://localhost:27017/')
db = client['nbadiscord']

CHANNEL_ID = "451745193457221642"

def log(c, user, userid, mod, reason="",):
    payload = {'date': datetime.datetime.now(), 'type': c, 'user': user, 'id': userid, 'mod': mod, 'reason': reason}
    db['general'].insert_one(payload)


description = '/r/nba discord bot'


bot = commands.Bot(command_prefix='?', description=description)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.event
async def on_error(event, *args, **kwargs):
    db['discordError'].insert_one({'error': traceback.format_exc()})


@bot.command(pass_context=True)
async def getlogs(ctx, user:str):
    cursor = (db['general'].find({'id': user[2:-1]}))
    embed = discord.Embed(title="modlogs")
    for document in cursor:
        embed.add_field(name="date", value=document['date'].strftime("%B %d, %Y %H:%M:%S"))
        embed.add_field(name="user", value=document['user'])
        embed.add_field(name='by', value=document['mod'])
        embed.add_field(name='type', value=document['type'])
        if document['reason'] != "":
            embed.add_field(name="reason", value=document['reason'])
    await bot.say(embed=embed)

@bot.command(pass_context=True)
async def boxscore(ctx, team: str, date: str = ""):
    if date == "":
        date = datetime.datetime.now().strftime('%Y%m%d')
    else:
        d = [date.split('/')[0],date.split('/')[1],date.split('/')[2]]
        d[0] = d[0].zfill(2)
        d[1] = d[1].zfill(2)
        d[2] = '20' + d[2]
        date = d[2] + d[0] + d[1]
    gameid = ""
    url = 'http://data.nba.com/5s/json/cms/noseason/scoreboard/'+date+'/games.json'
    resp = requests.get(url=url)
    json_response = resp.json()
    visitor = True
    for game in json_response['sports_content']['games']['game']:
        if game['visitor']['team_key'] == team:
            gameid = game['id']
        elif game['home']['team_key'] == team:
            gameid = game['id']
            visitor = False
    if gameid != "":
        url = "http://data.nba.net/json/cms/noseason/game/"+date+"/"+gameid+"/boxscore.json"
        json_response = requests.get(url=url).json()
        lineheader = "Player".ljust(15)+ " Min   FG FT 3PT +/- OR Reb A Blk Stl TO Pts"
        print(lineheader)
        players = ""
        awayhome = 'visitor' if visitor else 'home'
        for item in json_response['sports_content']['game'][awayhome]['players']['player']:
            name = (item['first_name'][0] + ". " +  item['last_name']).ljust(15)
            minutes = ((item['minutes']).zfill(2)+":"+(item['seconds']).zfill(2)).ljust(5)
            fg = item['field_goals_made'].ljust(2)
            ft = item['free_throws_made'].ljust(2)
            threes = item['three_pointers_made'].ljust(3)
            plusminus = item['plus_minus'].ljust(3)
            oreb = item['rebounds_offensive'].ljust(2)
            reb = str(int(item['rebounds_offensive']) + int(item['rebounds_defensive'])).ljust(3)
            assists = item['assists'].ljust(2)
            blocks = item['blocks'].ljust(3)
            steals = item['steals'].ljust(3)
            turnovers = item['turnovers'].ljust(2)
            points = item['points'].ljust(2)

            line = " ".join([name, minutes, fg, ft, threes, plusminus, oreb, reb, assists, blocks, steals, turnovers, points])
            players += line+"\n"
            print(line)
    await bot.say("```"+lineheader+"\n"+players+"```")

@bot.command(pass_context=True)
async def standings(ctx):
    url = 'http://data.nba.com/json/cms/2017/standings/conference.json'
    resp = requests.get(url=url)
    data = resp.json()
    string = "East                  \t                  West\n----------------------\t----------------------\n"
    for i in range(15):
        easternconf = data['sports_content']['standings']['conferences']['East']
        westernconf = data['sports_content']['standings']['conferences']['West']
        string += ((str(i+1) + " ").ljust(4) +easternconf['team'][i]['name'] + " " + easternconf['team'][i]['nickname']).ljust(23)+'\t'
        string += ((str(i+1) + " ").ljust(4) +westernconf['team'][i]['name'] + " " + westernconf['team'][i]['nickname']).ljust(23)+'\n'
        string += ("    " + easternconf['team'][i]['team_stats']['wins'] + " - " + easternconf['team'][i]['team_stats']['losses'] +'').ljust(23)+'\t'
        string += ("    " + westernconf['team'][i]['team_stats']['wins'] + " - " + westernconf['team'][i]['team_stats']['losses']).ljust(23) +'\n'
    await bot.say("```"+string+"```")



@bot.command(pass_context=True)
async def scores(ctx, date: str = ""):
    if date == "":
        date = datetime.datetime.now().strftime('%Y%m%d')
    else:
        d = [date.split('/')[0],date.split('/')[1],date.split('/')[2]]
        d[0] = d[0].zfill(2)
        d[1] = d[1].zfill(2)
        d[2] = '20' + d[2]
        date = d[2] + d[0] + d[1]
    url = 'http://data.nba.com/5s/json/cms/noseason/scoreboard/'+date+'/games.json'
    resp = requests.get(url=url)
    json_response = resp.json()
    embed = discord.Embed(title="scoreboard")
    for game in json_response['sports_content']['games']['game']:
        time24 = game['time']
        time = datetime.time(hour=int(time24[0:2]), minute=int(time24[2:4])).strftime('%I:%M %p')
        embed.add_field(name=game['game_url'].split('/')[1][0:3] + ' @ ' + game['game_url'].split('/')[1][3:],
                        value=game['visitor']['score'] + " - " + game['home']['score']+'\n'+
                              game['period_time']['period_status'] + " - " + game['period_time']['game_clock']+'\n'+time
                        )
    await bot.say(embed=embed)


@bot.command(pass_context=True)
async def addrole(ctx, *, rolerequested: str):
    member = ctx.message.author.name
    roles = ctx.message.server.roles
    for r in roles:
        if rolerequested == r.name:
            await bot.add_roles(member, r)
            await bot.say("added user to " + rolerequested)
            log(c='addrole', user=member, mod='bot')


@bot.command(pass_context=True)
async def removerole(ctx, *, rolerequested: str):
    member = ctx.message.author.name
    roles = ctx.message.server.roles
    for r in roles:
        if rolerequested == r.name:
            await bot.remove_roles(member, r)
            await bot.say("removed user from " + rolerequested)


@bot.command(pass_context=True)
@commands.has_role("Big Collar Brand")
async def kick(ctx, user: discord.Member, reason: str = ""):
    try:
        log(c='kick', user=user.name, mod=ctx.message.author.name, reason=reason)
        await bot.kick(user)
    except discord.Forbidden:
        log(c='error', user=user.name, userid=user.id, mod=ctx.message.author.name, reason='unable to kick')


@bot.command(pass_context=True)
@commands.has_role("Big Collar Brand")
async def ban(ctx, user: discord.User, *, reason: str = ""):
    try:
        log(c='ban', user=user.name, userid=user.id, mod=ctx.message.author.name, reason=reason)
        await bot.ban(user)
    except discord.Forbidden:
        log(c='error', user=user.name, userid=user.id, mod=ctx.message.author.name, reason='unable to ban')


@bot.command(pass_context=True)
@commands.has_role("Big Collar Brand")
async def unban(ctx, username: str, *, reason: str = ""):
    bannedusers = await bot.get_bans(ctx.message.server)
    for user in bannedusers:
        if username[2:-1] == user.id or username[1:-5] == user.name:
            try:
                log(c='unban', user=user.name, userid=user.id, mod=ctx.message.author.name, reason=reason)
                await bot.unban(user=user,server=ctx.message.server)
            except discord.Forbidden:
                log(c='error', user=user.name, userid=user.id, mod=ctx.message.author.name, reason="unable to unban")


@bot.command(pass_context=True)
@commands.has_role("Big Collar Brand")
async def getbanned(ctx):
    bannedusers = await bot.get_bans(ctx.message.server)
    for user in bannedusers:
        await bot.send_message(ctx.message.channel, user)

GAME_STATUS_PRE = 0
GAME_STATUS_IN = 1
GAME_STATUS_POST = 2

POS_TEN = 2
POS_HALF = 3
POS_PTEN = 4
POS_PFIVE = 5
POS_PTWO = 6
POS_FINAL = 7


def _returntime(delta):
    # Current Time will always be adjusted backwards 4 hours (4 am eastern will change over the day, allowing games to finish.)
    _currenttime = datetime.now() + timedelta(days=delta) + timedelta(hours=-4)

    return _addfrontzero(_currenttime.year) + _addfrontzero(_currenttime.month) + _addfrontzero(_currenttime.day)


def _addfrontzero(value):
    if value < 10:
        return "0" + str(value)
    else:
        return str(value)


# CINCIS CODE
#
#
#
#
#
#
#
#
#
#
#


BOT_PREFIX = ("?", "!")

teams = ["Toronto", "Boston", "Philadelphia", "Cleveland", "Indiana", "Miami", "Milwaukee Bucks", "Washington",
         "Detroit", "Charlotte", "New-York", "Brooklyn", "Chicago", "Orlando", "Atlanta", "Houston", "Golden State",
         "Portland", "Utah", "New Orleans", "San Antonio", "Oklahoma City", "Minnesota", "Denver", "LA", "Los Angeles",
         "Sacramento", "Dallas", "Memphis", "Phoenix"]
teams_short = ["TOR", "BOS", "PHI", "CLE", "IND", "MIA", "MIL", "WSH", "DET", "CHA", "NY", "BKN", "CHI", "ORL", "ATL",
               "HOU", "GS", "POR", "UTAH", "NO", "SA", "OKC", "MIN", "DEN", "LAC", "LAL", "SAC", "DAL", "MEM", "PHX"]
avaliable_roles = ["Game Night"]
cbbid = "363172066431598595"



########################################
# Score Bot Commands
# cinciforthewin
# Last Updated: 4/29/19
########################################

@bot.command()
async def allgames():
    try:
        gamestring = ''
        for game in espnscrape.scrapeESPN(0):
            gamestring = gamestring + game['team1'] + " " + str(game['score1']) + " vs. " + game['team2'] + " " + str(
                game['score2']) + " - " + game['time'] + "(TV: " + game['network'] + ")\n"
        await bot.say(gamestring)
    except Exception as e:
        print(str(e))
        await bot.say("Error Occured")




@bot.command()
async def score(*, team: str):
    try:
        if team not in teams and team not in teams_short:
            await bot.say("Can't Find Team")
            print("Can't Find Team")
        else:
            allgames = espnscrape.scrapeESPN(0)
            if team in (game['team1'] for game in allgames) or team in (game['team2'] for game in allgames) or team in (
                    game['team1abv'] for game in allgames) or team in (game['team2abv'] for game in allgames):
                for game in allgames:
                    if team == game['team1'] or team == game['team2'] or team == game['team1abv'] or team == game[
                        'team2abv']:
                        gamestring = game['team2'] + " " + str(game['score2']) + " @ " + game['team1'] + " " + str(
                            game['score1']) + " - " + game['time'] + " (TV: " + game['network'] + ")"
                        await bot.say(gamestring)
                        print(gamestring)
                        break
            else:
                allgameteam = '**Past Games:**\n'
                for n in range(-7, 7):
                    allgames = espnscrape.scrapeESPN(n)
                    if team in (game['team1'] for game in allgames) or team in (game['team2'] for game in
                                                                                allgames) or team in (game['team1abv']
                                                                                                      for game in
                                                                                                      allgames) or team in (
                            game['team2abv'] for game in allgames):
                        for game in allgames:
                            if team == game['team1'] or team == game['team2'] or team == game['team1abv'] or team == \
                                    game['team2abv']:
                                gamestring = game['team2'] + " " + str(game['score2']) + " @ " + game[
                                    'team1'] + " " + str(game['score1']) + " - " + game['time'] + " (TV: " + game[
                                                 'network'] + ")"
                                allgameteam = allgameteam + gamestring + "\n"
                                print(gamestring)
                    if n == 0:
                        allgameteam = allgameteam + "**Future Games:**\n"

                await bot.say("There are currently no games today.\n" + allgameteam)
    # print("No Games")
    except Exception as e:
        print(str(e))
        await bot.say("Error Occured")


@bot.command()
async def schedule(*, team: str):
    try:
        if team not in teams and team not in teams_short:
            await bot.say("Can't Find Team")
            print("Can't Find Team")
        else:
            allgames = espnscrape.scrapeESPN(0)
            allgameteam = '**Past Games:**\n'
            for n in range(-7, 7):
                gameday = False
                allgames = espnscrape.scrapeESPN(n)
                if team in (game['team1'] for game in allgames) or team in (game['team2'] for game in
                                                                            allgames) or team in (game['team1abv'] for
                                                                                                  game in
                                                                                                  allgames) or team in (
                        game['team2abv'] for game in allgames):
                    for game in allgames:
                        if team == game['team1'] or team == game['team2'] or team == game['team1abv'] or team == game[
                            'team2abv']:
                            gamestring = game['team2'] + " " + str(game['score2']) + " @ " + game['team1'] + " " + str(
                                game['score1']) + " - " + game['time'] + " (TV: " + game['network'] + ")"
                            gameday = True
                            allgameteam = allgameteam + gamestring + "\n"
                            print(gamestring)
                if n == -1:
                    allgameteam = allgameteam + "**Today's Game:**\n"
                elif n == 0:
                    # print("Enter 0 if")
                    # print(gameday)
                    if not gameday:
                        # print("entered if not statement")
                        allgameteam = allgameteam.replace("**Today's Game:**\n", "")

                    allgameteam = allgameteam + "**Future Game:**\n"

            await bot.say("Basketball Schedule for " + team + "\n" + allgameteam)
    # print("No Games")
    except Exception as e:
        print(str(e))
        await bot.say("Error Occured")



async def list_servers():
    await bot.wait_until_ready()
    while not bot.is_closed:
        print("Current servers:")
        for server in bot.servers:
            print(server.name)
        await asyncio.sleep(600)


async def liveticker():
    await bot.wait_until_ready()
    channel = discord.Object(id=CHANNEL_ID)
    while not bot.is_closed:
        try:
            await bot.send_message(channel, espnscrape.ScoreTickBuilder())
            await asyncio.sleep(30)
        except KeyboardInterrupt:
            exit()
        except Exception as e:
            await asyncio.sleep(30)
            continue





bot.loop.create_task(list_servers())
bot.loop.create_task(liveticker())

with open('token', 'r') as myfile:
    data = myfile.read().strip()
    bot.run(data)
