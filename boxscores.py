import datetime
import requests

@bot.command(pass_context=True)
async def scores(ctx, date: str = ""):
    gvteam = "HOU"
    gameid = ""
    url = 'http://data.nba.com/5s/json/cms/noseason/scoreboard/20180103/games.json'
    resp = requests.get(url=url)
    json_response = resp.json()
    for game in json_response['sports_content']['games']['game']:
        if game['visitor']['team_key'] == team or game['home']['team_key'] == team:
            gameid = game['id']
    if gameid != "":
        url = "http://data.nba.net/json/cms/noseason/game/20180103/"+gameid+"/boxscore.json"
        json_response = requests.get(url=url).json()
        lineheader = "Player".ljust(15)+ " Min   FG FT 3PT +/- OR Reb A Blk Stl TO Pts"
        print(lineheader)
        for item in json_response['sports_content']['game']['visitor']['players']['player']:
            name = (item['first_name'][0] + ". " +  item['last_name']).ljust(15)
            minutes = (item['minutes']+":"+item['seconds']).ljust(5)
            fg = item['field_goals_made'].ljust(2)
            ft = item['free_throws_made'].ljust(2)
            threes = item['three_pointers_made'].ljust(3)
            plusminus = item['plus_minus'].ljust(3)
            oreb = item['rebounds_offensive'].ljust(2)
            reb = str(int(item['rebounds_offensive']) + int(item['rebounds_defensive'])).ljust(3)
            assists = item['assists'].ljust(2)
            blocks = item['blocks'].ljust(3)
            steals = item['steals'].ljust(3)
            turnovers = item['turnovers'].ljust(2)
            points = item['points'].ljust(2)

            line = " ".join([name, minutes, fg, ft, threes, plusminus, oreb, reb, assists, blocks, steals, turnovers, points])

    await bot.say("```"+lineheader+"\n"+line+"```")